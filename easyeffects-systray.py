#!/bin/env python
import subprocess
import os
import shutil
from pystray import Icon as icon, Menu as menu, MenuItem as item
import cairosvg
from plyer import notification
from PIL import Image
from io import BytesIO
from sys import exit


def check_easyeffects_installed():
    return shutil.which("easyeffects") is not None


def get_easyeffects_presets():
    xdg_config_home = os.environ.get("XDG_CONFIG_HOME", os.path.expanduser("~/.config"))
    presets_dir = os.path.join(xdg_config_home, "easyeffects/output")
    if not os.path.isdir(presets_dir):
        return []
    presets = sorted(
        [
            os.path.splitext(preset)[0]
            for preset in os.listdir(presets_dir)
            if preset.endswith(".json")
        ],
        key=str.casefold
    )
    return presets


def set_easyeffects_preset(preset_name):
    subprocess.run(["easyeffects", "-l", str(preset_name)], check=True)

    notification.notify(
        title="EasyEffects Preset Switcher",
        message=f"Preset changed to: {preset_name}",
        app_name="EasyEffects Preset Switcher",
    )


def open_easyeffects():
    subprocess.Popen(["easyeffects"])


def create_menu_item(preset_name):
    def menu_item_callback(icon, preset_name=preset_name):
        set_easyeffects_preset(preset_name)

    return item(preset_name, menu_item_callback)


def update_presets(icon):
    icon.menu = create_menu()
    icon.update_menu()


def create_menu():
    presets = get_easyeffects_presets()
    menu_items = [create_menu_item(preset) for preset in presets]

    if not presets:
        menu_items.append(item("No presets found. Please create a preset in EasyEffects.", lambda icon: None, enabled=False))

    menu_items.append(menu.SEPARATOR)
    menu_items.append(item("Open EasyEffects", lambda icon: open_easyeffects()))
    menu_items.append(item("Refresh Preset List", update_presets))
    menu_items.append(item("Exit", lambda icon: exit(0), default=True))
    return menu(*menu_items)


def load_icon():
    icon_path = "/usr/share/icons/hicolor/scalable/apps/com.github.wwmm.easyeffects.svg"
    if not os.path.exists(icon_path):
        return Image.new("RGB", (64, 64), color="white")
    else:
        png_data = cairosvg.svg2png(url=icon_path)
        return Image.open(BytesIO(png_data))


def main():
    if not check_easyeffects_installed():
        print(
            "EasyEffects is not installed. Please install EasyEffects to use this script."
        )
        exit(1)

    icon_image = load_icon()

    tray_icon = icon(
        "EasyEffectsSwitcher", icon_image, "EasyEffects Preset Switcher", create_menu()
    )
    tray_icon.run()


if __name__ == "__main__":
    main()
