# EasyEffects Preset Switcher

This script provides a simple system tray application to switch between EasyEffects presets. It lists all available presets found in the EasyEffects configuration directory and allows users to switch between them with just a click. Additionally, it notifies the user each time a preset is changed.

## Features

- Lists all EasyEffects output presets.
- Allows switching between presets directly from the system tray.
- Displays a notification upon changing a preset.

## Requirements

- Python 3
- EasyEffects installed on your system
- The following Python packages: `pystray`, `Pillow`, `cairosvg`, `plyer`

## Installation

1. Ensure you have Python 3 installed on your system.
2. Install EasyEffects if you haven't already.
3. Clone this repository or download the script to your local machine.
4. Navigate to the script's directory and install the required Python packages by running:

```
pip install -r requirements.txt
```

## Usage
To run the EasyEffects Preset Switcher, navigate to the script's directory in your terminal and execute:

```
python easyeffects-systray.py
```

The script will add an icon to your system tray. Clicking this icon will display a menu with all available EasyEffects presets. Selecting a preset from this menu will apply it and display a notification confirming the change.

You probably want to add this script to your system startup applications. Please refer to your desktop environment documentation for that.

## Note
The script assumes that the EasyEffects configuration directory is located at ~/.config/easyeffects/output. If your configuration is located elsewhere, you may need to modify the script accordingly.
The script attempts to load an icon from /usr/share/icons/hicolor/scalable/apps/com.github.wwmm.easyeffects.svg. If this file does not exist, a default white icon will be used instead.

## License

MIT License

Copyright (c) 2024 Michael Koch

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
